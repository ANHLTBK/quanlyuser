﻿using ManagerStudentData.DataAccess.Interfaces;
using ManagerStudentData.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentData.DataAccess.Implements
{
    public class UserRepository : IUserRepository
    {
        public async Task<bool> AddOrChange(User user)
        {
            try
            {
                using(var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(user.Id)) // kiểm tra xem có bị rỗng không
                    {    //trả về phần tử đầu tiên của danh sách nếu tìm thấy
                        var userExits = dbContext.User.Where(x => x.Id.Equals(user.Id)).FirstOrDefault();
                        if(userExits != null)
                        {
                            // Update
                            userExits = user;
                            dbContext.User.Update(user);
                        }
                        else
                        {
                            // Add
                            dbContext.User.Add(user);
                        }

                        var result = dbContext.SaveChanges();

                        return result != -1;
                    }

                    return false;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        // tìm kiếm User kiểu dữ liệu trả về là 1 đối tượng User
        public async Task<User> GetUser(string username)
        {
            try
            {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(username)) // kiểm tra xem có bị rỗng không
                    {    //trả về phần tử đầu tiên của danh sách nếu tìm thấy
                        var userExitsName = dbContext.User.Where(x => x.UserName.Equals(username)).FirstOrDefault();
                        if (userExitsName != null)
                        {
                            return userExitsName;
                        }
                        else
                        {
                            return null;
                        }
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //delete 
        public async Task DeleteUser(string id)
        {
            try
            {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(id))
                    { 
                        var userExitsid = dbContext.User.Where(x => x.Id.Equals(id)).FirstOrDefault();
                        if (userExitsid != null)
                        {
                            dbContext.User.Remove(userExitsid);
                            dbContext.SaveChanges();

                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
         }
        //update
        public async Task<User> UpdateUser(User user)
        {
            try {
                using(var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(user.Id)) // kiểm tra xem có bị rỗng không
                    {    //trả về phần tử đầu tiên của danh sách nếu tìm thấy
                        var userExits = dbContext.User.Where(x => x.Id.Equals(user.Id)).FirstOrDefault();
                        if (userExits != null)
                        {
                            // update
                            userExits.FullName = user.FullName;
                            userExits.Email = user.Email;
                            userExits.Phone = user.Phone;
                            userExits.UserName = user.UserName;
                            //
                            dbContext.User.Update(userExits);
                            var result = dbContext.SaveChanges();
                            return userExits;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        // get all user
        public async Task<List<User>> GetAllUser()
        {

            try
            {
                using (var dbContext = new ManagerStudentContext())
                {
                    var getUser = dbContext.User.ToList();
                    if (getUser != null)
                    {
                        return getUser;
                    }
                    else
                    {
                        return null;
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

    }
}
