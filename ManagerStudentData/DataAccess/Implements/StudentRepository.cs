﻿using ManagerStudentData.DataAccess.Interfaces;
using ManagerStudentData.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentData.DataAccess.Implements
{
    public class StudentRepository:IStudentRepository
    {
        public async Task<bool> AddStudent(Student st)
        {
            try {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(st.Id))
                    {
                        var studentExits = dbContext.Student.Where(x => x.Id.Equals(st.Id)).FirstOrDefault();
                        if(studentExits != null) {
                            studentExits = st;
                            dbContext.Student.Update(st);
                        }
                        else
                        {
                            dbContext.Add(st);
                        }
                        var result = dbContext.SaveChanges();

                        return result != -1;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<Student>> GetAllStudent()
        {
            try {
                using(var dbContext = new ManagerStudentContext())
                {
                    var getStudent = dbContext.Student.ToList();
                    if (getStudent != null)
                    {
                        return getStudent;
                    }else
                    {
                        return null;
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Student> GetStudent(string name)
        {
            try {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(name))
                    {
                        var studentExits = dbContext.Student.Where(x => x.Name.Equals(name)).FirstOrDefault();
                        if (studentExits != null)
                        {
                            return studentExits;
                        }
                        else
                        {
                            return null;

                        }
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Student> UpDateStudent(Student student)
        {
            try {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(student.Id))
                    {
                        var studentExits = dbContext.Student.Where(x => x.Id.Equals(student.Id)).FirstOrDefault();
                        if(studentExits != null)
                        {
                            studentExits.Mssv = student.Mssv;
                            studentExits.Name = student.Name;
                            studentExits.Phone = student.Email;
                            studentExits.Address = student.Address;

                            dbContext.Student.Update(studentExits);
                            var result = dbContext.SaveChanges();
                            return studentExits;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task DeleteStudent(string id)
        {
            try {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        var studentExits = dbContext.Student.Where(x => x.Id.Equals(id)).FirstOrDefault();
                        if(studentExits != null)
                        {
                            dbContext.Student.Remove(studentExits);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
