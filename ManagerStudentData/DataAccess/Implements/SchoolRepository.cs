﻿using ManagerStudentData.DataAccess.Interfaces;
using ManagerStudentData.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentData.DataAccess.Implements
{
    public class SchoolRepository:ISchoolRepository
    {
        public async Task<bool> AddSchool(School school)
        {
            try {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(school.Id))
                    {
                        var schoolExits = dbContext.School.Where(x => x.Id.Equals(school.Id)).FirstOrDefault();
                        if(schoolExits != null)
                        {
                            schoolExits = school;
                            dbContext.Update(school);
                        }
                        else
                        {
                            dbContext.Add(school);
                        }
                        var result = dbContext.SaveChanges();
                        return result != -1;
                    }
                    return false;
                } 
            }
            catch(Exception ex)
            {
                throw ex;
            }
           
        }
        public async Task<List<School>> GetAllSchool()
        {
            try {
                using(var dbContext = new ManagerStudentContext())
                {
                    var getschool = dbContext.School.ToList();
                    if (getschool != null)
                    {
                        return getschool;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<School> GetSchool(string name)
        {
            try { 
                using(var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(name))
                    {
                        var schoolExit = dbContext.School.Where(x => x.Name.Equals(name)).FirstOrDefault();
                        if(schoolExit != null)
                        {
                            return schoolExit;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<School> UpdateSchool(School school)
        {
            try
            {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(school.Id))
                    {
                        var schoolExits = dbContext.School.Where(x => x.Id.Equals(school.Id)).FirstOrDefault();
                        if(schoolExits != null)
                        {
                            schoolExits.Name = school.Name;
                            schoolExits.Phone = school.Phone;
                            schoolExits.Address = school.Address;
                            dbContext.School.Update(schoolExits);
                            dbContext.SaveChanges();
                            return schoolExits;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task DeleteSchool(string id)
        {
            try {
                using(var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        var schoolExits = dbContext.School.Where(x => x.Id.Equals(id)).FirstOrDefault();
                        if(schoolExits != null)
                        {
                            dbContext.School.Remove(schoolExits);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
