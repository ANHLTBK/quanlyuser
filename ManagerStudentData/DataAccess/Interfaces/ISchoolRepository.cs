﻿using ManagerStudentData.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentData.DataAccess.Interfaces
{
    public interface ISchoolRepository
    {
        Task<bool> AddSchool(School school);
        Task<List<School>> GetAllSchool();
        Task<School> GetSchool(string username);
        Task DeleteSchool(string id);
        Task<School> UpdateSchool(School school);
    }
}
