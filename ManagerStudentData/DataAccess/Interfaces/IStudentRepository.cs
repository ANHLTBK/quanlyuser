﻿using ManagerStudentData.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentData.DataAccess.Interfaces
{
    public interface IStudentRepository
    {
        Task<bool> AddStudent(Student st);
        Task<List<Student>> GetAllStudent();
        Task<Student> GetStudent(string name);
        Task DeleteStudent(string id);
        Task<Student> UpDateStudent(Student student);
    }
}
