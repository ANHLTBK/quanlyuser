﻿using ManagerStudentData.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentData.DataAccess.Interfaces
{
    public interface IUserRepository
    {
        Task<bool> AddOrChange(User user);
        Task<User> GetUser(string username);
        Task<List<User>> GetAllUser();
        Task DeleteUser(string id);
        Task<User> UpdateUser(User user);
    }
}
