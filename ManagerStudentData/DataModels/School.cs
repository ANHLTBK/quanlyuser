﻿using System;
using System.Collections.Generic;

namespace ManagerStudentData.DataModels
{
    public partial class School
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
