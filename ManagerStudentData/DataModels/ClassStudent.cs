﻿using System;
using System.Collections.Generic;

namespace ManagerStudentData.DataModels
{
    public partial class ClassStudent
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int? StudentNumber { get; set; }
    }
}
