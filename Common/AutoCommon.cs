﻿using ManagerStudentData.DataModels;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Common
{
    public class AutoCommon
    {
        public static string GenToken(TokenModel model, string secret, string issuer, string audience, int expire)
        {
            try
            {
                // xác định xem có phải token do chính cái hệ thống này sinh ra hay không
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
                //thông tin đăng nhập
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                // tao claim 
                List<Claim> claims = new List<Claim>()
                {
                    new Claim("email", model.Email),
                    new Claim("username", model.UserName)
                };
                //
                var tokeOptions = new JwtSecurityToken(
                    issuer: issuer,
                    audience: audience,
                    claims: claims,
                    expires: DateTime.Now.AddHours(expire),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);

                return tokenString;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        } 
    }
}
