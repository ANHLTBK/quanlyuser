﻿using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Interfaces;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.Services.Responses;
using ManagerStudentService.WDataModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLySinhVien.Controllers.Student
{
    [Route("[controller]/[action]")]
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;
        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }
        // add
        [HttpPost, Authorize]
        public async Task<StudentResponses> AddStudent([FromBody] WStudent student)
        {
            try
            {
                StudentResponses reponse = new StudentResponses();
                var result = await _studentService.AddStudent(student);
                if (result)
                {
                    reponse.Status = true;
                }
                else
                {
                    reponse.Status = false;
                }
                return reponse;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        // delete
        [HttpDelete,Authorize]
        public void DeleteStudent([FromBody] StudentDeleteRequest request)
        {
            try {
                _studentService.DeleteStudent(request);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        //Update
        [HttpPost,Authorize]
        public async Task<RStudent> UpDateStudent([FromBody] WStudent student)
        {
            try {
                var result = await _studentService.UpDateStudent(student);
                if(result != null)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //getAll
        [HttpGet,Authorize]
        public async Task<List<RStudent>> GetAllSudent()
        {
            try{
                var restul = await _studentService.GetAllStudent();
                if(restul != null)
                {
                    return restul;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
