﻿using Common;
using ManagerStudentService.Mappings;
using ManagerStudentService.Services.Interfaces;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.Services.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVien.Controllers.User
{
    [Route("[controller]/[action]")]
    public class UserLoginController : Controller
    {
        private readonly IUserService _userService;
        private readonly ILoginService _loginService;
        private readonly IConfiguration _configuration;

        public UserLoginController(IUserService userService, IConfiguration configuration,
                                   ILoginService loginService)
        {
            _userService = userService;
            _configuration = configuration;
            _loginService = loginService;
        }

        [HttpPost]
        public async Task<LoginResponses> Login([FromBody] LoginRequest request)
        {
            try
            {
                var response = await _loginService.Login(request);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
