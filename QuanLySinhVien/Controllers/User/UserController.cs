﻿using ManagerStudentData.DataModels;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Interfaces;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.Services.Responses;
using ManagerStudentService.WDataModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuanLySinhVien.Controllers.User
{
    [Route("[controller]/[action]")]
    public class UserController: Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        // add or change
        [HttpPost]
        public async Task<UserAddOrChangeResponse> AddOrChange([FromBody] WUser user)
        {
            try
            {
                UserAddOrChangeResponse response = new UserAddOrChangeResponse();
                var result = await _userService.AddOrChange(user);

                if (result)
                {
                    response.Status = true;
                }
                else
                {
                    response.Status = false;
                }
                return response;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        //update
        [HttpPost, Authorize]
        public async Task<RUser> Update([FromBody] WUser request)
        {
            try
            {
                var result = await _userService.UpdateUser(request);

                if (result != null)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //delete
        [HttpPost, Authorize]
        public void DeleteUser([FromBody] UserDeleteRequest request)
        {
            try {
                _userService.DeleteUser(request);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        // get list
        [HttpGet,Authorize]
        public async Task<List<RUser>> GetAllUser()
        {
            try {
                var result = await _userService.GetAllUser();
                if(result != null)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
