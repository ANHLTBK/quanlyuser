﻿using System;
using System.Collections.Generic;
using System.Linq;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.Services.Responses;
using System.Threading.Tasks;
using ManagerStudentService.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using ManagerStudentService.WDataModels;
using ManagerStudentService.RDataModels;
using Microsoft.AspNetCore.Authorization;

namespace QuanLySinhVien.Controllers
{
    [Route("[controller]/[action]")]
    public class SchoolController : Controller
    {
        private readonly ISchoolService _schoolService;
        public SchoolController(ISchoolService schoolService)
        {
            _schoolService = schoolService;
        }
        // add
        [HttpPost, Authorize]
        public async Task<SchoolResponse> AddSchool([FromBody] WSchool school)
        {
            try {
                SchoolResponse response = new SchoolResponse();
                var result = await _schoolService.AddSchool(school);
                if (result)
                {
                    response.Status = true;
                }
                else
                {
                    response.Status = false;
                }
                return response;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        // delete
        [HttpDelete, Authorize]
        public void DeleteSchool([FromBody] SchoolDeleteRequest school)
        {
            try {
                _schoolService.DeleteSchool(school);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        //Update
        [HttpPost, Authorize]
        public async Task<RSchool> UpdateSchool([FromBody] WSchool school)
        {
            try {
                var result = await _schoolService.UpdateSchool(school);
                if (result != null)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        //getAll
        [HttpGet, Authorize]
        public async Task<List<RSchool>> GetAll()
        {
            try {
                var result = await _schoolService.GetAllSchool();
                if(result != null)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
             
    }
}