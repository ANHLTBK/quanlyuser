﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.WDataModels
{
    public class WSchool
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
