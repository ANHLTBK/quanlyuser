﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.RDataModels
{
    public class RSchool
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
