﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.RDataModels
{
    public class RUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
