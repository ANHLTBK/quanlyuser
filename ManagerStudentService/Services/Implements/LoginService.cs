﻿using Common;
using ManagerStudentService.Mappings;
using ManagerStudentService.Services.Interfaces;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.Services.Responses;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentService.Services.Implements
{
    public class LoginService : ILoginService
    {
        private readonly IUserService _userService;
        private IConfiguration _configuration;

        public LoginService(IUserService userService, IConfiguration configuration)
        {
            _userService = userService;
            _configuration = configuration;
        }

        public async Task<LoginResponses> Login(LoginRequest request)
        {
            try
            {
                LoginResponses response = new LoginResponses();
                var rUser = await _userService.LoginUser(request);

                if (rUser != null)
                {
                    var tokenModel = rUser.ToModel();
                    string tokenString = AutoCommon.GenToken(tokenModel, _configuration["SecretToken"],
                                                             _configuration["issuer"], _configuration["audience"], 5);
                    response.Token = tokenString;
                    response.Status = true;
                    return response;
                }
                else
                {
                    response.Status = false;
                }
                return response;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
