﻿using ManagerStudentData.DataAccess.Interfaces;
using ManagerStudentData.DataModels;
using ManagerStudentService.Mappings;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Interfaces;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.WDataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentService.Services.Implements
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<bool> AddOrChange(WUser user)
        {
            try
            {
                var userData = user.ToModel();
                var result = await _userRepository.AddOrChange(userData);
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<RUser> UpdateUser(WUser user)
        {
            try
            {
                if (!string.IsNullOrEmpty(user.Id))
                {
                    var userData = user.ToModel();
                    var userExits = await _userRepository.UpdateUser(userData);
                    return userExits.ToModel();
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task DeleteUser(UserDeleteRequest request)
        {
            try {
                await _userRepository.DeleteUser(request.UserID);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<RUser> LoginUser(LoginRequest request)
        {
            try
            {
                var userExits = await _userRepository.GetUser(request.UserName);

                if(userExits != null)
                {
                    if (!string.IsNullOrEmpty(request.Password))
                    {
                        if (userExits.Password.Equals(request.Password))
                        {
                            return userExits.ToModel();
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<RUser>> GetAllUser()
        {
            try {
                var users = await _userRepository.GetAllUser();
                if(users != null)
                {
                    return users.Select(x => x.ToModel()).ToList();
                }
                return null;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
