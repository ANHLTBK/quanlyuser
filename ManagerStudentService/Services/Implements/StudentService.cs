﻿using ManagerStudentData.DataModels;
using System;
using ManagerStudentService.Mappings;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManagerStudentData.DataAccess.Interfaces;
using ManagerStudentService.WDataModels;
using ManagerStudentService.Services.Interfaces;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Requests;

namespace ManagerStudentService.Services.Implements
{
    public class StudentService :IStudentService
    {
        private readonly IStudentRepository _studentRepository;
        public StudentService(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public async Task<bool> AddStudent(WStudent st)
        {
            try
            {
                var StudentData = st.ToModel();
                var result = await _studentRepository.AddStudent(StudentData);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<RStudent> UpDateStudent(WStudent student)
        {
            try {
                using (var dbContext = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(student.Id))
                    {
                        var studentData = student.ToModel();
                        var studentExits = await _studentRepository.UpDateStudent(studentData);
                        return studentExits.ToModel();
                    }
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task DeleteStudent(StudentDeleteRequest request)
        {
            try {
                await _studentRepository.DeleteStudent(request.StudentID);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<RStudent>> GetAllStudent()
        {
            try {
                var studentExits = await _studentRepository.GetAllStudent();
                if(studentExits != null)
                {
                    return studentExits.Select(x => x.ToModel()).ToList();
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
