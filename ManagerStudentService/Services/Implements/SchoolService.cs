﻿using ManagerStudentData.DataAccess.Interfaces;
using ManagerStudentData.DataModels;
using ManagerStudentService.Mappings;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Interfaces;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.WDataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentService.Services.Implements
{
   public class SchoolService:ISchoolService
    {
        private readonly ISchoolRepository _schoolRepository;
        public SchoolService(ISchoolRepository schoolRepository)
        {
            _schoolRepository = schoolRepository;
        }
        public async Task<List<RSchool>> GetAllSchool()
        {
            try
            {
                var schoolExit = await _schoolRepository.GetAllSchool();
                if(schoolExit != null)
                {
                    return schoolExit.Select(x => x.ToModel()).ToList();
                }
                return null;
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        public async Task<RSchool> UpdateSchool(WSchool wSchool)
        {
            try {
                using (var dbContex = new ManagerStudentContext())
                {
                    if (!string.IsNullOrEmpty(wSchool.Id))
                    {
                        // schoolExit chinh la doi tuong School, 
                        // vi  UpdateSchool tra ve 1 School => schoolExit la 1 doi tuong
                        // nen schoolExit se khop voi ToModel ,vi RSchool nhan dau vao la 1 School
                        var schoolData = wSchool.ToModel();
                        var schoolExit = await _schoolRepository.UpdateSchool(schoolData);
                        return schoolExit.ToModel();
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task DeleteSchool(SchoolDeleteRequest schoolDeleteRequest)
        {
            try {
                await _schoolRepository.DeleteSchool(schoolDeleteRequest.SchoolID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> AddSchool(WSchool wSchool)
        {
            try {
                var schoolData = wSchool.ToModel();
                var result = await _schoolRepository.AddSchool(schoolData);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
