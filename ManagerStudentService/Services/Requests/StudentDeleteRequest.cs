﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Services.Requests
{
    public class StudentDeleteRequest
    {
        public string StudentID { get; set; }
    }
}
