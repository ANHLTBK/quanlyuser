﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Services.Requests
{
    public class LoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
