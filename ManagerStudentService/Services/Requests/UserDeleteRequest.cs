﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Services.Requests
{
    public class UserDeleteRequest
    {
        public string UserID { get; set; }
    }
}
