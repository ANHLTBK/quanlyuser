﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Services.Requests
{
    public class SchoolDeleteRequest
    {
        public string SchoolID { get; set; }
    }
}
