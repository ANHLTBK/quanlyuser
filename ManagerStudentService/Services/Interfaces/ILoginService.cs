﻿using ManagerStudentService.Services.Requests;
using ManagerStudentService.Services.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentService.Services.Interfaces
{
    public interface ILoginService
    {
        Task<LoginResponses> Login(LoginRequest request);
    }
}
