﻿using ManagerStudentData.DataModels;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.WDataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentService.Services.Interfaces
{
    public interface IUserService
    {
        Task<bool> AddOrChange(WUser user);
        Task<RUser> LoginUser(LoginRequest request);
        Task<List<RUser>> GetAllUser();
        Task DeleteUser(UserDeleteRequest request);
        Task<RUser> UpdateUser(WUser user);

    }
}
