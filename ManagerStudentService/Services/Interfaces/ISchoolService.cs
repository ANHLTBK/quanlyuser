﻿using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.WDataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentService.Services.Interfaces
{
   public interface ISchoolService
    {
        Task<bool> AddSchool(WSchool wSchool);
        Task<List<RSchool>> GetAllSchool();
        Task<RSchool> UpdateSchool(WSchool wSchool);
        Task DeleteSchool(SchoolDeleteRequest schoolDeleteRequest);
    }
}
