﻿using ManagerStudentData.DataModels;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.WDataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagerStudentService.Services.Interfaces
{
    public interface IStudentService
    {
        Task<bool> AddStudent(WStudent student);
        Task<List<RStudent>> GetAllStudent();
        Task DeleteStudent(StudentDeleteRequest request);
        Task<RStudent> UpDateStudent(WStudent request);
    }
}
