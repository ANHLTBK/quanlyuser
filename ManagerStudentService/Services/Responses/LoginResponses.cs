﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Services.Responses
{
    public class LoginResponses: BaseResponse
    {
        public string Token { get; set; }
    }
}
