﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Services.Responses
{
    public class BaseResponse
    {
        public bool Status { get; set; }
    }
}
