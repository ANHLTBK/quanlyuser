﻿using ManagerStudentData.DataModels;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.WDataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Mappings
{
    public static class StudentMapping
    {
        public static Student ToModel(this WStudent model)
        {
            if (model == null)
                return null;
            return new Student()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Email = model.Email ?? string.Empty,
                Mssv = model.MSSV ?? string.Empty,
                Phone = model.Phone ?? string.Empty,
                Name = model.Name ?? string.Empty,
                Address = model.Address ?? string.Empty,
            };
        }
        public static WStudent ToModel(this StudentAddRequest model)
        {
            if (model == null)
                return null;
            return new WStudent()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Email = model.Email ?? string.Empty,
                MSSV = model.MSSV ?? string.Empty,
                Phone = model.Phone ?? string.Empty,
                Name = model.Name ?? string.Empty,
                Address = model.Address ?? string.Empty,
            };
        }
        public static RStudent ToModel(this Student model)
        {
            if (model == null)
                return null;
            return new RStudent()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Email = model.Email ?? string.Empty,
                MSSV = model.Mssv ?? string.Empty,
                Phone = model.Phone ?? string.Empty,
                Name = model.Name ?? string.Empty,
                Address = model.Address ?? string.Empty,
            };
        }
    }
}
