﻿using Common;
using ManagerStudentData.DataModels;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.WDataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Mappings
{
    public static class UserMapping
    {
        public static User ToModel(this WUser model)
        {
            if (model == null)
                return null;
            return new User()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Email = model.Email ?? string.Empty,
                FullName = model.FullName ?? string.Empty,
                Phone = model.Phone ?? string.Empty,
                UserName = model.UserName ?? string.Empty,
                Password = model.Password ?? string.Empty,
            };
        }
        public static RUser ToModel(this User model)
        {
            if (model == null)
                return null;
            return new RUser()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Email = model.Email ?? string.Empty,
                FullName = model.FullName ?? string.Empty,
                Phone = model.Phone ?? string.Empty,
                UserName = model.UserName ?? string.Empty,
                Password = model.Password ?? string.Empty,
            };
        }
        public static TokenModel ToModel(this RUser model)
        {
            if (model == null)
                return null;
            return new TokenModel()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Email = model.Email ?? string.Empty,
                FullName = model.FullName ?? string.Empty,
                Phone = model.Phone ?? string.Empty,
                UserName = model.UserName ?? string.Empty,
                Password = model.Password ?? string.Empty,
            };
        }
        public static WUser ToModel(this UserAddOrChangeRequest model)
        {
            if (model == null)
                return null;
            return new WUser()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Email = model.Email ?? string.Empty,
                FullName = model.FullName ?? string.Empty,
                Phone = model.Phone ?? string.Empty,
                UserName = model.UserName ?? string.Empty,
                Password = model.Password ?? string.Empty,
            };
        }
    }
}
