﻿using ManagerStudentData.DataModels;
using ManagerStudentService.RDataModels;
using ManagerStudentService.Services.Requests;
using ManagerStudentService.WDataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerStudentService.Mappings
{
   public static class SchoolMaping
    {
        public static RSchool ToModel(this School model)
        {
            if (model == null)
                return null;
            return new RSchool()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Name = model.Name ?? string.Empty,
                Address = model.Address ?? string.Empty,
                Phone = model.Phone?? string.Empty
            };
        }
        public static School ToModel(this WSchool model)
        {
            if (model == null)
                return null;
            return new School()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Name = model.Name ?? string.Empty,
                Address = model.Address ?? string.Empty,
                Phone = model.Phone ?? string.Empty
            };
        }
        public static WSchool ToModel(this SchoolAddRequest model)
        {
            if (model == null)
                return null;
            return new WSchool()
            {
                Id = model.Id ?? Guid.NewGuid().ToString(),
                Name = model.Name ?? string.Empty,
                Address = model.Address ?? string.Empty,
                Phone = model.Phone ?? string.Empty
            };
        }
    }
}
